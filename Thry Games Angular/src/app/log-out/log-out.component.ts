import { Component } from '@angular/core';
import { ThryService } from '../thry.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-log-out',
  templateUrl: './log-out.component.html',
  styleUrls: ['./log-out.component.css']
})
export class LogOutComponent {

  loginStatus:any;
  
  constructor(private router: Router, private service: ThryService) {
    this.service.setUserLoggedOut();
    this.loginStatus = false; // Update loginStatus
    router.navigate(['signin']);
  }

}
