import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ThryService {

 
  isUserLogged: boolean;
  loginstatus:Subject<any>;
  cartItems:any
  
  constructor(private http : HttpClient) {
    this.isUserLogged=false;
    this.loginstatus=new Subject();
    this.cartItems=[];
   }

  getCountries() : any{
    return this.http.get('https://restcountries.com/v3.1/all');
  }

  getAllEmployes(){
    return this.http.get('http://localhost:8085/getCustomers')
  }

  getEmployeById(empId:any){
    return this.http.get('getCustomerById/'+empId)
  }

  register(employe:any){
    return this.http.post('registerCustomer',employe)
  }

  updateEmploye(employe :any){
    return this.http.put('updateCustomer',employe);
  }

  deleteEmploye(empId:any){
    return this.http.delete('deleteCustomerById/'+empId)
  }

  empLogin(employe:any):Observable<any>{
    return this.http.get('custLogin/'+employe.emailId+"/"+employe.password);
  }

  getLoginStatus(){
    return this.loginstatus.asObservable();
  }
  
   //Successfully Logged In
   setUserLoggedIn() {
    this.isUserLogged = true;
    this.loginstatus.next(true);
  }

   //Logout
  setUserLoggedOut() {
    this.isUserLogged = false;
    this.loginstatus.next(false)
  }

  getUserLoggedStatus(): boolean {
    return this.isUserLogged;
  }
  addToCart(product: any) {
    this.cartItems.push(product);
  }
  getCartItems(): any {
    return this.cartItems;
  }

  sendOtp(emailId: string): Observable<any> {
    return this.http.post('http://localhost:8085/emailOtp', { emailId: emailId });
  }

  verifyOtp(emailId: string, otp: number): Observable<any> {
    return this.http.post('http://localhost:8085/validateEmailOtp/${emailId}/${otp}', {});
  }

  setPassword(emailId: string, password: string): Observable<any> {
    return this.http.put('http://localhost:8085/setPassword', { emailId: emailId, password: password });
  }
}
