import { Component } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { ThryService } from '../thry.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent {
  emailId = "";
  currentStep: 'email' | 'otp' | 'setPassword';

  constructor(private service: ThryService, private toastr: ToastrService, private router: Router) {
    this.currentStep = 'email';
  }


  verifyEmail(emailForm: any) {
    this.emailId = emailForm.emailId;
    this.service.sendOtp(emailForm.emailId).subscribe(response => {
      if (response.message === 'OTP is sent !!!') {
        this.toastr.success("OTP Sent", "Success");
        this.currentStep = 'otp';
      } else {
        this.toastr.error("Email not Found", "Failed");
      }
      console.log(response);
    });
  }

  verifyOtp(otpForm: any) {
    this.service.verifyOtp(this.emailId, otpForm.otp).subscribe(response => {
      if (response.message === "OTP is valid !!!") {
        this.toastr.success("OTP Verified", "Success");
        this.currentStep = 'setPassword';
      } else {
        this.toastr.error("Invalid OTP", "Failed");
      }
      console.log(otpForm);
    });
  }

  setPassword(setPasswordForm: any) {
    this.service.setPassword(this.emailId, setPasswordForm.password).subscribe(response => {
      if (response.message === 'Password Updated !!!') {
        this.toastr.success("Password Updated", "Success");
        this.router.navigate(['login']);
      } else {
        this.toastr.error("Failed to Update", "Failed");
      }
      console.log(setPasswordForm);
    });
  }

  validateRepeat(password: string, rpassword: string): boolean {
    return password == rpassword;
  }
}
