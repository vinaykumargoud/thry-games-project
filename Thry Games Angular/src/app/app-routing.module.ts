import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SignInComponent } from './sign-in/sign-in.component';
import { DobComponent } from './dob/dob.component';
import { SingUpComponent } from './sing-up/sing-up.component';

const routes: Routes = [
  {path:"", component:DobComponent},
  {path:"dob", component:DobComponent},
  {path:"sign-in", component: SignInComponent},
  {path:"sing-up", component:SingUpComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
