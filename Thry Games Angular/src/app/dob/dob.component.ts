import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dob',
  templateUrl: './dob.component.html',
  styleUrls: ['./dob.component.css']
})
export class DobComponent  implements OnInit {

  showDateDropdown: boolean = false;
  showMonthDropdown: boolean = false;
  showYearDropdown: boolean = false;

  constructor(private router: Router) {}
  ngOnInit() {
  }

  selectedDate: number = 1; 
  selectedMonth: number = 1; 
  selectedYear: number = new Date().getFullYear();

  dates: number[] = Array.from({ length: 31 }, (_, i) => i + 1);
  months = [
    { name: 'January', value: 1 },
    { name: 'February', value: 2 },
    { name: 'March', value: 3 },
    { name: 'April', value: 4 },
    { name: 'May', value: 5 },
    { name: 'June', value: 6 },
    { name: 'July', value: 7 },
    { name: 'August', value: 8 },
    { name: 'September', value: 9 },
    { name: 'October', value: 10 },
    { name: 'November', value: 11 },
    { name: 'December', value: 12 },
  ];
  
  currentYear = new Date().getFullYear();
  years: number[] = Array.from({ length: 100 }, (_, i) => this.currentYear - 50 + i);

  toggleDropdowns() {
    this.showDateDropdown = true;
    this.showMonthDropdown = true;
    this.showYearDropdown = true;
  }

  checkAgeAndRedirect() {
    const currentDate = new Date();
    const selectedBirthDate = new Date(
      this.selectedYear,
      this.selectedMonth - 1, 
      this.selectedDate
    );
  const age = currentDate.getFullYear() - selectedBirthDate.getFullYear();
  if (age < 18) {
    alert("You must be 18 years or older to sign up.");
  } else {
    this.router.navigate(['sing-up']);
  }
  }

}
