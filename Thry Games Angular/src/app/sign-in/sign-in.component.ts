import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ThryService } from '../thry.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})
export class SignInComponent {

  emailId: string;
  password: string;
  employe:any;
  
  constructor(private router :Router,private toaster : ToastrService,private service :ThryService){
    this.emailId="";
    this.password="";
  }

  ngOnInit() {
  }

  validateMail(mail:any):boolean{
    return /[@]/.test(mail);
  }
  isCaptchaResolved: boolean = false;

  handleCaptchaResolved(event: any): void {
    this.isCaptchaResolved = true;
  }

  passwordValidator(password: string): boolean {

    return  /[A-Z]/.test(password) && /[!@#$%^&*()_+{}\[\]:;<>,.?~\\-]/.test(password) &&  /[0-9]/.test(password);

  }

 validateLogin(loginForm:any){
    console.log(loginForm)

    localStorage.setItem('emilId',loginForm.emailId);

    if(loginForm.emailId=='HR@gmail.com' && loginForm.password=='vinayhr'){
            this.toaster.success('Wwlcom HR, Here Are Your Employes')
      this.router.navigate(['showemps']);
      this.service.setUserLoggedIn();
    }else{

     this.service.empLogin(loginForm).subscribe(data =>{
        console.log(data);
        this.employe = data;
      })
      // this.router.navigate(['products'])
      // this.service.setUserLoggedIn();
    
    if (this.employe != null) {
      this.router.navigate(['products']);
      this.service.setUserLoggedIn();
    } else {
      alert('Invalid Creadentials');
    }
  }
}
}
