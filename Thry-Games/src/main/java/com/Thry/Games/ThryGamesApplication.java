package com.Thry.Games;

import javax.annotation.PostConstruct;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.bind.annotation.CrossOrigin;

import com.twilio.Twilio;
@EnableJpaRepositories(basePackages="com.dao")
@EntityScan(basePackages="com.model")
@SpringBootApplication(scanBasePackages="com")
@CrossOrigin(origins = "http://localhost:4200/")

public class ThryGamesApplication {

	public static final String ACCOUNT_SID = "AC7a0c63b317751f76052c9b3001aa601b";
	public static final String AUTH_TOKEN = "a036535eb52fdfa07e63e09d93ce6f16";
	
	public static void main(String[] args) {
		SpringApplication.run(ThryGamesApplication.class, args);
	}

	@PostConstruct
	public void initTwilio(){
		Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
	}
}
