package com.Thry.Games;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dao.CustomerDao;
import com.model.Customer;


@RestController
public class CustomerController {
	@Autowired
	CustomerDao customerDao;

	@GetMapping("getCustomers")
	public List<Customer> getCustomers() {
		return customerDao.getCustomers();
	}

	@GetMapping("getCustomerById/{mail}")
	public Customer getCustomerById(@PathVariable("mail") String mail) {
		return customerDao.getCustomerById(mail);
	}

	@GetMapping("check-email")
	public boolean checkEmailExists(@RequestParam("email") String email) {
		return customerDao.emailExists(email);
	}


	@GetMapping("getCustomerByName/{custName}")
	public Customer getCustomerByName(@PathVariable("custName") String CustomerName) {
		return customerDao.getCustomerByName(CustomerName);
	}

	@PostMapping("registerCustomer")
	public String registerCustomer(@RequestBody Customer customer) {
		customerDao.registerCustomer(customer);
		return "Customer Registered Successfully!!!";
	}

	@DeleteMapping("deleteCustomerById/{custId}")
	public String deleteCustomerById(@PathVariable("custId") String employeeId) {
		customerDao.deleteCustomerById(employeeId);
		return "Customer Deleted Successfully!!!";
	}

	@GetMapping("custLogin/{emailId}/{password}")
	public Customer custLogin(@PathVariable("emailId") String emailId, @PathVariable("password") String password) {
		Customer customer=customerDao.getCustomerByEmailId(emailId);
		if(customer != null){
			BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
			if(encoder.matches(password, customer.getPassword())){
				return customer;
			}
		}else{
			return null;
		}
		return customerDao.custLogin(emailId, password);
	}

	
	 @PostMapping("send")
	    public ResponseEntity<String> sendEmail(@RequestBody MessageRequest messageRequest) {
	        try {
	            emailService.sendEmail(messageRequest);
	            return ResponseEntity.ok("Email sent successfully!");
	        } catch (Exception e) {
	            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to send email.");
	        }
	    }

	@PostMapping("emailOtp")
	public ResponseEntity<Map<String, String>> EmailOtp(@RequestBody Customer employee) {
		Map <String, String> response = new HashMap<>();
		if(customerDao.EmailOtp(employee)) {
			response.put("message", "OTP is sent !!!");
		}
		else {
			response.put("message", "Invalid EmailId");
		}
		return ResponseEntity.ok(response);
	}


	@PostMapping("MobileOtp/{mobileNo}")
	public String otpPhoneNo(@PathVariable("mobileNo") String mobileNo){
		Customer cust = customerDao.getCustomerByMobileNo(mobileNo);
		if(cust != null){
			customerDao.generateMobileOtp(cust);
			return "OTP sent to : " + mobileNo;
		}
		return "PhoneNumber not Found";
	}

	@PostMapping("validateEmailOtp/{email}/{otp}")
	public ResponseEntity<Map<String, String>> validateEmailOtp(@PathVariable("email") String email, @PathVariable("otp") String otpStr) {
		Map<String, String> response = new HashMap<>();
		try {
			int otp = Integer.parseInt(otpStr); 
			if (customerDao.validateEmailOtp(email, otp)) {
				response.put("message", "OTP is valid !!!");
			} else {
				response.put("message", "Invalid OTP !!!");
			}
		} catch (NumberFormatException e) {
			response.put("message", "Invalid OTP format !!!");
		}
		return ResponseEntity.ok(response);
	}


	@PostMapping("validateMobileOtp/{mobileNo}/{otp}")
	public String validatePhoneOtp(@PathVariable("mobileNo") String mobileNo, @PathVariable("otp") int otp){
		if(customerDao.validateMobileOtp(mobileNo, otp)){
			return "OTP is valid !!!";
		}
		else {
			return "OTP is Invalid !!!";
		}
	}

	@PutMapping("setPassword")
	public ResponseEntity<Map<String, String>> setPassword(@RequestBody Customer employee) {
		Map <String, String> response = new HashMap<>();
		if(customerDao.setPassword(employee)) {
			response.put("message", "Password Updated !!!");
		}
		else {
			response.put("message", "Failed to Update Password");
		}
		return ResponseEntity.ok(response);
	}
}
