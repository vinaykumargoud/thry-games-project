package com.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import com.model.MessageRequest;

import org.springframework.mail.SimpleMailMessage;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMessage;

@Service
public class EmailService {

    @Autowired
    private JavaMailSender emailSender;

    public void sendEmail(MessageRequest messageRequest) throws MessagingException {
        MimeMessage message = emailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message, true);

        helper.setFrom("vinaykumar.kee@gmail.com");
        helper.setTo("vinaykumar.kee@gmail.com");
        helper.setSubject(messageRequest.getSubject());
        helper.setText("From: " + messageRequest.getEmail() + "\n\n" + messageRequest.getMessage());

        emailSender.send(message);
    }
}

