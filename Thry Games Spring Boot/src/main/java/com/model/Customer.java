package com.model;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Customer {

	private String name;
	private String gender;
	@Id
	private String email;
	private String country;
	private String mobileNo;
	private Date dateOfBirth;
	private String password;
	
	public Customer() {
		super();
	}
	
	
	public Customer(String email, String password) {
		super();
		this.email = email;
		this.password = password;
	}

	public Customer(String email) {
		super();
		this.email = email;
	}

	public Customer( String name, String gender, String email, String country, String mobileNo,
			Date dateOfBirth, String password) {
		super();
		this.name = name;
		this.gender = gender;
		this.email = email;
		this.country = country;
		this.mobileNo = mobileNo;
		this.dateOfBirth = dateOfBirth;
		this.password = password;
	}


	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}

	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}

	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	
}
