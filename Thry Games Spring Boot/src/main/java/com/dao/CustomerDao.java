package com.dao;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.model.Customer;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

@Service
public class CustomerDao {

	@Autowired
	CustomerRepository customerRepository;
	@Autowired
	JavaMailSender mailSender;
	@Autowired
	CustomerOtp customerOtp;

	public List<Customer> getCustomers() {
		return customerRepository.findAll();
	}

	public Customer getCustomerById(String custId) {
		return customerRepository.findById(custId).orElse(new Customer( "Not Found","","","","",null,""));
	}

	public boolean emailExists(String email) {
		Customer customer = customerRepository.findByEmail(email);
		return customer != null;
	}

	public Customer getCustomerByName(String custName) {
		return customerRepository.findByName(custName);
	}
 
	public Customer registerCustomer(Customer cust) {
		if(customerRepository.existsById(cust.getEmail())){
			return null;
		}
			BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
			String bcryptHash = encoder.encode(cust.getPassword());
			cust.setPassword(bcryptHash);
			return customerRepository.save(cust);	
	}

	public void deleteCustomerById(String custId) {
		customerRepository.deleteById(custId);
	}	

	public Customer custLogin(String emailId, String password) {
		return customerRepository.empLogin(emailId, password);
	}
	
	public Customer getCustomerByEmailId(String emailId) {
		return customerRepository.findByEmail(emailId);
	}
	
	public boolean EmailOtp(Customer employee) {
		Customer emp = customerRepository.findByEmail(employee.getEmail());
		if(emp != null) {
			int otp = (int) (Math.random() * 900000) + 100000;
			LocalDateTime expiryTime = LocalDateTime.now().plusMinutes(3);
			customerOtp.setEmailOtp(otp);
			customerOtp.setEmailOtpExpiryTime(expiryTime);
			SimpleMailMessage message = new SimpleMailMessage();
			message.setTo(emp.getEmail());
			message.setSubject("Your OTP Code");
			message.setText("Your Thry Games Email Verification OTP code is " + otp + " is valid only for 3 minutes \n"
					+ "Thank You For Choosing Thry Games");

			mailSender.send(message);
			return true;
		}
		return false;
	}


	public boolean validateEmailOtp(String email, int otp) {
		Customer cust = customerRepository.findByEmail(email);
		if(cust != null) {
			if(customerOtp.getEmailOtp() == otp && customerOtp.getEmailOtpExpiryTime().isAfter(LocalDateTime.now())) {
				customerOtp.setEmailOtp(0);
				customerOtp.setEmailOtpExpiryTime(null);
				return true;
			}
		}
		return false;
	}

	public boolean setPassword(Customer employee) {
		Customer cust = customerRepository.findByEmail(employee.getEmail());
		if(cust != null) {
			BCryptPasswordEncoder bcpe = new BCryptPasswordEncoder() ;
			String bcryptpw = bcpe.encode(employee.getPassword());
			cust.setPassword(bcryptpw);
			customerRepository.save(cust);
			return true;
		}
		return false;
	}

	public Customer getCustomerByMobileNo(String phoneNo) {
		return customerRepository.findByMobileNo(phoneNo);
	}

	public void generateMobileOtp(Customer cust) {
		int otp = (int) (Math.random() * 9000) + 1000;
		LocalDateTime expiryTime = LocalDateTime.now().plusMinutes(3);
		customerOtp.setMobileOtp(otp);
		customerOtp.setMobileOtpExpiryTime(expiryTime);
		Message.creator(new PhoneNumber(cust.getMobileNo()), new PhoneNumber("+13345183269"),
				" Your Thry Games Mobile Verification OTP is : " + otp+" is valid only 3 minutes \n"
						+ "Thank You For Choosing Thry Games").create();
	}

	public boolean validateMobileOtp(String mobileNo, int otp) {

		Customer cust = customerRepository.findByMobileNo(mobileNo);
		if(cust != null){
			if(customerOtp.getMobileOtp() == otp && customerOtp.getMobileOtpExpiryTime().isAfter(LocalDateTime.now())){
				customerOtp.setMobileOtp(0);
				customerOtp.setMobileOtpExpiryTime(null);
				return true;
			}
		}
		return false;
	}
}

