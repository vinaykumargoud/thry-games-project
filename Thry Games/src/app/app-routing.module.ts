import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SignInComponent } from './sign-in/sign-in.component';
import { SingUpComponent } from './sing-up/sing-up.component';
import { HomeComponent } from './home/home.component';
import { authGuard } from './auth.guard';
import { SignOutComponent } from './sign-out/sign-out.component';
import { GameComponent } from './game/game.component';
import { ContactComponent } from './contact/contact.component';
import { PremiumComponent } from './premium/premium.component';

const routes: Routes = [
  { path: "", component: HomeComponent },
  { path: "home", component: HomeComponent },
  { path: "signup", component: SingUpComponent},
  { path: "signin", component: SignInComponent },
  { path: "signout", canActivate:[authGuard], component: SignOutComponent },
  { path: "game", component: GameComponent },
  { path: "contact", component: ContactComponent },
  { path: "premium", component: PremiumComponent },
  {
    path: 'game/:id',  
    component: GameComponent,
  },
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
