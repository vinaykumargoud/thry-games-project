import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';
import * as $ from 'jquery';

@Component({
  selector: 'app-premium',
  templateUrl: './premium.component.html',
  styleUrls: ['./premium.component.css']
})
export class PremiumComponent {

  showNavigationArrows = true;
  showNavigationIndicators = true;
  images = [
    'assets/img/page-top-bg/3.jpg','assets/img/slider-1.jpg', 'assets/img/slider-2.jpg'
  ];
  recentReviews = [
    {
      background: 'assets/img/review-bg.png',
      title: "Overwatch Halloween",
      score: 9.3,
      rating: [true, true, true, true, false],
      description: 'Halloween Terror is an annual seasonal event. The event has exclusive cosmetics, and an exclusive brawl, Junkensteins Revenge.',
      cover: 'assets/img/review/5.jpg',
    },
    {
      background: 'assets/img/review-bg.png',
      title: 'Grand Theft Auto',
      score: 9.3,
      rating: [true, true, true, true, false],
      description: 'The Grand Theft Auto V: Premium Edition includes the complete GTAV story, Grand Theft Auto Online and all existing gameplay upgrades and content.',
      cover: 'assets/img/review/6.jpg',
    },
    {
      background: 'assets/img/review-bg.png',
      title: 'Avatar',
      score: 9.3,
      rating: [true, true, true, true, false],
      description: 'Avatar: Frontiers of Pandora is a first-person action-adventure game that happens in a massive open world similar to those found in the Assassins Creed and Far Cry game series from Ubisoft.',
      cover: 'assets/img/review/7.jpg',
    },
    {
      background: 'assets/img/review-bg.png',
      title: 'Anthem',
      score: 9.3,
      rating: [true, true, true, true, false],
      description: 'Set on a fictional planet named Coda, players assume the role of Freelancers, heroic adventurers who wear powerful exosuits to defend humanity from the threats beyond their cities walls.',
      cover: 'assets/img/review/8.jpg',
    },
    {
      background: 'assets/img/review-bg.png',
      title: 'Cyberpunk 2077',
      score: 9.3,
      rating: [true, true, true, true, false],
      description: 'Cyberpunk 2077 is an open-world, action-adventure RPG set in the megalopolis of Night City, where you play as a cyberpunk mercenary wrapped up in a do-or-die fight for survival.',
      cover: 'assets/img/review/9.jpg',
    },
    {
      background: 'assets/img/review-bg.png',
      title: 'Spiderman',
      score: 9.3,
      rating: [true, true, true, true, false],
      description: 'Man is an open-world third-person action-adventure game, in which the player controls Peter Parker, under his superhero identity Spider-Man, through Manhattan, New York City to fight crime.',
      cover: 'assets/img/review/10.jpg',
    },
  ];
  recentReviews2 = [
    {
      background: 'assets/img/review-bg-2.jpg',
      category: 'new',
      title: "Assasin’s Creed",
      score: 9.3,
      description: 'Depicts a fictional millennia-old struggle between the Order of Assassins, who fight for peace and free will, and the Knights Templar, who desire peace through order and control. ',
      cover: 'assets/img/review/1.jpg',
    },
    {
      background: 'assets/img/review-bg-2.jpg',
      category: 'new',
      title: 'Doom',
      score: 9.5,
      description: 'The player assumes the role of a space marine, later unofficially referred to as Doomguy, fighting through hordes of undead humans and invading demons.',
      cover: 'assets/img/review/2.jpg',
    },
    {
      background: 'assets/img/review-bg-2.jpg',
      category: 'new',
      title: 'Overwatch',
      score: 9.1,
      description: 'Overwatch is a multimedia franchise centered on a series of online multiplayer first-person shooter (FPS) video game',
      cover: 'assets/img/review/3.jpg',
    },
    {
      background: 'assets/img/review-bg-2.jpg',
      category: 'new',
      title: 'Lords of the Fallen ',
      score: 9.7,
      description: 'Lords of the Fallen is an action role-playing video game played from a third-person perspective.',
      cover: 'https://cdn2.unrealengine.com/egs-lords-of-the-fallen-carousel-desktop-1920x1080-8edcd525f1fc.jpg',
    },
  ];
  constructor(config: NgbCarouselConfig, private router: Router) {
    config.showNavigationArrows = true;
    config.showNavigationIndicators = true;
    config.interval = 5000;
    config.wrap = true;
  }

  ngOnInit() {
  }
}
