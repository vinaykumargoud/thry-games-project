import { HttpClient } from '@angular/common/http';
import { Component, OnInit, AfterViewInit } from '@angular/core';
import * as $ from 'jquery';
import { ToastrService } from 'ngx-toastr';
import { ThryService } from '../thry.service';
import { Router } from '@angular/router';

declare var google: any; 

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit, AfterViewInit {

  commentForm:any={};

  loginStatus: any;

  constructor(private http: HttpClient,private toaster:ToastrService,private messageService: ThryService,private router:Router) {
   this.loginStatus = this.messageService.getUserLoggedStatus();
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    // Initialize Google Maps inside ngAfterViewInit
    const myOptions = {
      zoom: 16,
      center: new google.maps.LatLng(51.489500, -0.096777), // Change the coordinates
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      scrollwheel: false,
      mapTypeControl: false,
      zoomControl: false,
      streetViewControl: false
    };

    const map = new google.maps.Map(document.getElementById('map-canvas'), myOptions);
    const marker = new google.maps.Marker({
      map: map,
      position: new google.maps.LatLng(51.489500, -0.096777) // Change the coordinates
    });

    google.maps.event.addListener(marker, 'click', function() {
      const infowindow = new google.maps.InfoWindow({
        content: 'Your info here'
      });
      infowindow.open(map, marker);
    });

    // Add the code for preloader inside ngAfterViewInit
    $(window).on('load', () => {
      /*------------------
        Preloder
      --------------------*/
      $(".loader").fadeOut();
      $("#preloder").delay(400).fadeOut("slow");

      /* Other code inside the window load event */
    });
  }

  onSubmit(commentForm: any) {
      if (this.loginStatus) {
      this.messageService.sendMessage(commentForm).subscribe(
        (response: any) => {
          if (response.status === 200) {
            this.toaster.success('Email sent successfully!');
          } else {
            this.toaster.success('Email sent successfully!');
          }
        }
      );
    }else {
      this.toaster.error('User Must Sign In To Download');
      this.router.navigate(['signin']);
    }
  }

  toastermsg(){
    this.toaster.success('Email sent successfully!')
  }
}
