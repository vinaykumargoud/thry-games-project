import { ChangeDetectorRef, Component, OnInit, TemplateRef } from '@angular/core';
import { ThryService } from '../thry.service';
import { NgbCarouselConfig, NgbOffcanvas } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';


@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  loginStatus: any;

  showNavigationArrows = true;
	showNavigationIndicators = true;
	images = [
    'https://c4.wallpaperflare.com/wallpaper/1000/316/848/spider-man-2018-game-wallpaper-preview.jpg',
    'https://c4.wallpaperflare.com/wallpaper/586/507/755/gta-5-gta-v-games-4k-wallpaper-preview.jpg',
    'https://c4.wallpaperflare.com/wallpaper/169/715/816/pubg-playerunknowns-battlegrounds-2018-games-games-wallpaper-preview.jpg'
  ];
  constructor(private service: ThryService,config: NgbCarouselConfig,private router:Router,private offcanvasService: NgbOffcanvas) {
    config.showNavigationArrows = true;
		config.showNavigationIndicators = true;
    config.interval = 5000;
    config.wrap = true;
  }
  // toggleTheme() {
  //   this.service.setTheme('light'); // or 'dark'
  // }
  ngOnInit() {
     this.service.getLoginStatus().subscribe((data: any) => {
       this.loginStatus = data;
     });
  }

  isNavigationAllowed(): boolean {
    const currentRoute = this.router.url;
    return !currentRoute.includes('/signin') && !currentRoute.includes('/signup');
  }

  openScroll(content: TemplateRef<any>) {
		this.offcanvasService.open(content, { scroll: true });
	}

}
