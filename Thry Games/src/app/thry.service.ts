import { OverlayContainer } from '@angular/cdk/overlay';
import { HttpClient } from '@angular/common/http';
import { Injectable, Renderer2, RendererFactory2 } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ThryService {
  isUserLogged: boolean;
  loginStatus: Subject<any>;

  
  constructor(private http : HttpClient,private overlayContainer: OverlayContainer,private rendererFactory: RendererFactory2) {
    this.isUserLogged = false;
    this.loginStatus = new Subject();
   }

  getLoginStatus() {
    return this.loginStatus.asObservable();
  }
  setUserLoggedIn() {
    this.isUserLogged = true;
    this.loginStatus.next(true);
  }
  setUserLoggedOut() {
    this.isUserLogged = false;
    this.loginStatus.next(false);
  }
  getUserLoggedStatus(): boolean {
    return this.isUserLogged;
  }

  getCountries() : any{
    return this.http.get('https://restcountries.com/v3.1/all');
  }

  getAllCustomers(){
    return this.http.get('getCustomers')
  }
  getCustomerById(custId:any){
    return this.http.get('getCustomerById/'+custId)
  }
  register(customer:any):Observable<any>{
    return this.http.post('registerCustomer',customer)
  }
  checkIfEmailExists(email: string): Observable<boolean> {
    return this.http.get<boolean>(`check-email?email=${email}`);
 }
  update(customer :any){
    return this.http.put('updateCustomer',customer);
  }
  delete(custId:any){
    return this.http.delete('deleteCustomerById/'+custId)
  }
  Login(customer:any){
    return this.http.get('custLogin/'+customer.email+"/"+customer.password).toPromise();
  }



  sendOtp(email: string): Observable<any> {
    return this.http.post('emailOtp', { email: email });
  }
  verifyOtp(email: string, otp: any): Observable<any> {
    return this.http.post(`validateEmailOtp/${email}/${otp}`, {});
  }
  setPassword(email: string, password: string): Observable<any> {
    return this.http.put('setPassword', { email: email, password: password });
  }
sendMessage(messageData: any): Observable<any> {
  return this.http.post('send', messageData);
}

//  for themes.....

}
